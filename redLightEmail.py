import email, smtplib,ssl

from email import encoders
from email.mime.base import MIMEBase
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

subject = "An email with attachment from Python"
body = "Red Light Triggered"
sender_email = "cpe185.redlight@gmail.com"
receiver_email = "ekv.gamboa@gmail.com"
password = "bullshit185-"

#create multipart and header
message = MIMEMultipart()
message["From"] = sender_email
message["To"] = receiver_email
message["Subject"] = subject

#add body to email
message.attach(MIMEText(body,"plain"))

filename = "/home/pi/Desktop/redlight.jpg" #directory to image

#open jpg file in binary mode
with open(filename,"rb") as attachment:
	part = MIMEBase("application","octect-stream")
	part.set_payload(attachment.read())

#encode file in ASCII characters
encoders.encode_base64(part)

#add header as key/value pair to attachment part
part.add_header("Content-Disposiition",f"attachment; filename = {filename}",)

#add attachment to message and convert to string
message.attach(part)
text = message.as_string()

#log in to server using secure context and send email
context = ssl.create_default_context()
with smtplib.SMTP_SSL("smtp.gmail.com",465,context=context) as server:
	server.login(sender_email,password)
	server.sendmail(sender_email,receiver_emal,text)