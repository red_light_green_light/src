# PiCamera imports
from picamera import PiCamera
from time import sleep

#external mod imports
import RPi.GPIO as GPIO
import time
#pin definitions
cameraSwitch= 17 #pin 17/Pi pin 11
redLight = 22 # pin 22 / Pi pin 15

#pin setup
GPIO.setmode(GPIO.BCM)
GPIO.setup(cameraSwitch,GPIO.IN)
GPIO.setup(redLight,GPIO.IN)

#initial state
#GPIO.output(cameraSwitch,GPIO.LOW)

#camera setup
camera = PiCamera()
print("testing camera. Press CTRL+C to ext")
try:
    while 1:
        #if GPIO.input(redLight,GPIO.HIGH) and GPIO.input(cameraSwitch,GPIO.HIGH):
        if GPIO.input(cameraSwitch):
            camera.start_preview()
            sleep(2)
            camera.capture('/home/pi/Desktop/redlight.jpg')
            camera.stop_preview()
except KeyboardInterrupt:
    GPIO.cleanup()
