/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2019 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
UART_HandleTypeDef huart2;

/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_USART2_UART_Init(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int w;
typedef enum
{
Transition_NS_State,
NS_Pass_EW_Stop_State,
All_Stop_EW_State,
NS_Stop_EW_Pass_State,
Transition_SN_State,
SN_Pass_EW_Stop_State,
All_Stop_SN_State,
Transition_EW_State,
All_Stop_NS_State,
Transition_Cross_State,
All_Stop_Cross_State,
Cross_State,
Transition_Cross_EW_State
}eSystemState;
/* Prototype Event Handlers */
eSystemState NorthSouthPassHandler(void)
{
HAL_GPIO_WritePin(GPIOB, NS_GREEN_LED_Pin, GPIO_PIN_SET);
HAL_GPIO_WritePin(GPIOB, NS_YELLOW_LED_Pin, GPIO_PIN_RESET);
HAL_GPIO_WritePin(GPIOA, NS_RED_LED_Pin, GPIO_PIN_RESET);
HAL_GPIO_WritePin(GPIOA, EW_GREEN_LED_Pin, GPIO_PIN_RESET);
HAL_GPIO_WritePin(GPIOA, EW_YELLOW_LED_Pin, GPIO_PIN_RESET);
HAL_GPIO_WritePin(GPIOA, EW_RED_LED_Pin, GPIO_PIN_SET);
HAL_GPIO_WritePin(GPIOA, SN_GREEN_LED_Pin, GPIO_PIN_RESET);
HAL_GPIO_WritePin(GPIOA, SN_YELLOW_LED_Pin, GPIO_PIN_RESET);
HAL_GPIO_WritePin(GPIOA, SN_RED_LED_Pin, GPIO_PIN_SET);
HAL_GPIO_WritePin(GPIOB, CROSS_GREEN_LED_Pin, GPIO_PIN_RESET);
HAL_GPIO_WritePin(GPIOB, CROSS_RED_LED_Pin, GPIO_PIN_SET);
HAL_Delay(5*1000); //5 seconds
return Transition_NS_State;
}
eSystemState TransitionNorthSouthHandler(void)
{
HAL_GPIO_WritePin(GPIOB, NS_GREEN_LED_Pin, GPIO_PIN_RESET);
HAL_GPIO_WritePin(GPIOB, NS_YELLOW_LED_Pin, GPIO_PIN_SET);
HAL_GPIO_WritePin(GPIOA, NS_RED_LED_Pin, GPIO_PIN_RESET);
HAL_GPIO_WritePin(GPIOA, EW_GREEN_LED_Pin, GPIO_PIN_RESET);
HAL_GPIO_WritePin(GPIOA, EW_YELLOW_LED_Pin, GPIO_PIN_RESET);
HAL_GPIO_WritePin(GPIOA, EW_RED_LED_Pin, GPIO_PIN_SET);
HAL_GPIO_WritePin(GPIOA, SN_GREEN_LED_Pin, GPIO_PIN_RESET);
HAL_GPIO_WritePin(GPIOA, SN_YELLOW_LED_Pin, GPIO_PIN_RESET);
HAL_GPIO_WritePin(GPIOA, SN_RED_LED_Pin, GPIO_PIN_SET);
HAL_GPIO_WritePin(GPIOB, CROSS_GREEN_LED_Pin, GPIO_PIN_RESET);
HAL_GPIO_WritePin(GPIOB, CROSS_RED_LED_Pin, GPIO_PIN_SET);
HAL_Delay(5*1000); //5 seconds
return All_Stop_EW_State;
}
eSystemState AllStopEastWestHandler(void)
{
HAL_GPIO_WritePin(GPIOB, NS_GREEN_LED_Pin, GPIO_PIN_RESET);
HAL_GPIO_WritePin(GPIOB, NS_YELLOW_LED_Pin, GPIO_PIN_RESET);
HAL_GPIO_WritePin(GPIOA, NS_RED_LED_Pin, GPIO_PIN_SET);
HAL_GPIO_WritePin(GPIOA, EW_GREEN_LED_Pin, GPIO_PIN_RESET);
HAL_GPIO_WritePin(GPIOA, EW_YELLOW_LED_Pin, GPIO_PIN_RESET);
HAL_GPIO_WritePin(GPIOA, EW_RED_LED_Pin, GPIO_PIN_SET);
HAL_GPIO_WritePin(GPIOA, SN_GREEN_LED_Pin, GPIO_PIN_RESET);
HAL_GPIO_WritePin(GPIOA, SN_YELLOW_LED_Pin, GPIO_PIN_RESET);
HAL_GPIO_WritePin(GPIOA, SN_RED_LED_Pin, GPIO_PIN_SET);
HAL_GPIO_WritePin(GPIOB, CROSS_GREEN_LED_Pin, GPIO_PIN_RESET);
HAL_GPIO_WritePin(GPIOB, CROSS_RED_LED_Pin, GPIO_PIN_SET);
HAL_Delay(5*1000); //5 seconds
return NS_Stop_EW_Pass_State;
}
eSystemState EastWestPassHandler(void)
{
HAL_GPIO_WritePin(GPIOB, NS_GREEN_LED_Pin, GPIO_PIN_RESET);
HAL_GPIO_WritePin(GPIOB, NS_YELLOW_LED_Pin, GPIO_PIN_RESET);
HAL_GPIO_WritePin(GPIOA, NS_RED_LED_Pin, GPIO_PIN_SET);
HAL_GPIO_WritePin(GPIOA, EW_GREEN_LED_Pin, GPIO_PIN_SET);
HAL_GPIO_WritePin(GPIOA, EW_YELLOW_LED_Pin, GPIO_PIN_RESET);
HAL_GPIO_WritePin(GPIOA, EW_RED_LED_Pin, GPIO_PIN_RESET);
HAL_GPIO_WritePin(GPIOA, SN_GREEN_LED_Pin, GPIO_PIN_RESET);
HAL_GPIO_WritePin(GPIOA, SN_YELLOW_LED_Pin, GPIO_PIN_RESET);
HAL_GPIO_WritePin(GPIOA, SN_RED_LED_Pin, GPIO_PIN_SET);
HAL_GPIO_WritePin(GPIOB, CROSS_GREEN_LED_Pin, GPIO_PIN_RESET);
HAL_GPIO_WritePin(GPIOB, CROSS_RED_LED_Pin, GPIO_PIN_SET);
HAL_Delay(5*1000); //5 seconds
return Transition_EW_State;
}
eSystemState TransitionEastWestNSHandler(void)
{
HAL_GPIO_WritePin(GPIOB, NS_GREEN_LED_Pin, GPIO_PIN_RESET);
HAL_GPIO_WritePin(GPIOB, NS_YELLOW_LED_Pin, GPIO_PIN_RESET);
HAL_GPIO_WritePin(GPIOA, NS_RED_LED_Pin, GPIO_PIN_SET);
HAL_GPIO_WritePin(GPIOA, EW_GREEN_LED_Pin, GPIO_PIN_RESET);
HAL_GPIO_WritePin(GPIOA, EW_YELLOW_LED_Pin, GPIO_PIN_SET);
HAL_GPIO_WritePin(GPIOA, EW_RED_LED_Pin, GPIO_PIN_RESET);
HAL_GPIO_WritePin(GPIOA, SN_GREEN_LED_Pin, GPIO_PIN_RESET);
HAL_GPIO_WritePin(GPIOA, SN_YELLOW_LED_Pin, GPIO_PIN_RESET);
HAL_GPIO_WritePin(GPIOA, SN_RED_LED_Pin, GPIO_PIN_SET);
HAL_GPIO_WritePin(GPIOB, CROSS_GREEN_LED_Pin, GPIO_PIN_RESET);
HAL_GPIO_WritePin(GPIOB, CROSS_RED_LED_Pin, GPIO_PIN_SET);
HAL_Delay(5*1000); //5 seconds
return All_Stop_NS_State;
}
eSystemState TransitionEastWestSNHandler(void)
{

HAL_GPIO_WritePin(GPIOB, NS_GREEN_LED_Pin, GPIO_PIN_RESET);
HAL_GPIO_WritePin(GPIOB, NS_YELLOW_LED_Pin, GPIO_PIN_RESET);
HAL_GPIO_WritePin(GPIOA, NS_RED_LED_Pin, GPIO_PIN_SET);
HAL_GPIO_WritePin(GPIOA, EW_GREEN_LED_Pin, GPIO_PIN_RESET);
HAL_GPIO_WritePin(GPIOA, EW_YELLOW_LED_Pin, GPIO_PIN_SET);
HAL_GPIO_WritePin(GPIOA, EW_RED_LED_Pin, GPIO_PIN_RESET);
HAL_GPIO_WritePin(GPIOA, SN_GREEN_LED_Pin, GPIO_PIN_RESET);
HAL_GPIO_WritePin(GPIOA, SN_YELLOW_LED_Pin, GPIO_PIN_RESET);
HAL_GPIO_WritePin(GPIOA, SN_RED_LED_Pin, GPIO_PIN_SET);
HAL_GPIO_WritePin(GPIOB, CROSS_GREEN_LED_Pin, GPIO_PIN_RESET);
HAL_GPIO_WritePin(GPIOB, CROSS_RED_LED_Pin, GPIO_PIN_SET);
HAL_Delay(5*1000); //5 seconds
return All_Stop_SN_State;
}
eSystemState AllStopNorthSouthHandler(void)
{
HAL_GPIO_WritePin(GPIOB, NS_GREEN_LED_Pin, GPIO_PIN_RESET);
HAL_GPIO_WritePin(GPIOB, NS_YELLOW_LED_Pin, GPIO_PIN_RESET);
HAL_GPIO_WritePin(GPIOA, NS_RED_LED_Pin, GPIO_PIN_SET);
HAL_GPIO_WritePin(GPIOA, EW_GREEN_LED_Pin, GPIO_PIN_RESET);
HAL_GPIO_WritePin(GPIOA, EW_YELLOW_LED_Pin, GPIO_PIN_RESET);
HAL_GPIO_WritePin(GPIOA, EW_RED_LED_Pin, GPIO_PIN_SET);
HAL_GPIO_WritePin(GPIOA, SN_GREEN_LED_Pin, GPIO_PIN_RESET);
HAL_GPIO_WritePin(GPIOA, SN_YELLOW_LED_Pin, GPIO_PIN_RESET);
HAL_GPIO_WritePin(GPIOA, SN_RED_LED_Pin, GPIO_PIN_SET);
HAL_GPIO_WritePin(GPIOB, CROSS_GREEN_LED_Pin, GPIO_PIN_RESET);
HAL_GPIO_WritePin(GPIOB, CROSS_RED_LED_Pin, GPIO_PIN_SET);
HAL_Delay(5*1000); //5 seconds
return NS_Pass_EW_Stop_State;
}
eSystemState SouthNorthHandler(void)
{
	HAL_GPIO_WritePin(GPIOB, NS_GREEN_LED_Pin, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(GPIOB, NS_YELLOW_LED_Pin, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(GPIOA, NS_RED_LED_Pin, GPIO_PIN_SET);
	HAL_GPIO_WritePin(GPIOA, EW_GREEN_LED_Pin, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(GPIOA, EW_YELLOW_LED_Pin, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(GPIOA, EW_RED_LED_Pin, GPIO_PIN_SET);
	HAL_GPIO_WritePin(GPIOA, SN_GREEN_LED_Pin, GPIO_PIN_SET);
	HAL_GPIO_WritePin(GPIOA, SN_YELLOW_LED_Pin, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(GPIOA, SN_RED_LED_Pin, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(GPIOB, CROSS_GREEN_LED_Pin, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(GPIOB, CROSS_RED_LED_Pin, GPIO_PIN_SET);
	HAL_Delay(5*1000); //5 seconds
	return Transition_SN_State;
}
eSystemState TransitionSouthNorthHandler(void)
{
	HAL_GPIO_WritePin(GPIOB, NS_GREEN_LED_Pin, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(GPIOB, NS_YELLOW_LED_Pin, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(GPIOA, NS_RED_LED_Pin, GPIO_PIN_SET);
	HAL_GPIO_WritePin(GPIOA, EW_GREEN_LED_Pin, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(GPIOA, EW_YELLOW_LED_Pin, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(GPIOA, EW_RED_LED_Pin, GPIO_PIN_SET);
	HAL_GPIO_WritePin(GPIOA, SN_GREEN_LED_Pin, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(GPIOA, SN_YELLOW_LED_Pin, GPIO_PIN_SET);
	HAL_GPIO_WritePin(GPIOA, SN_RED_LED_Pin, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(GPIOB, CROSS_GREEN_LED_Pin, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(GPIOB, CROSS_RED_LED_Pin, GPIO_PIN_SET);
	HAL_Delay(5*1000); //5 seconds
	return All_Stop_EW_State;
}
eSystemState AllStopSNHandler(void)
{
	HAL_GPIO_WritePin(GPIOB, NS_GREEN_LED_Pin, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(GPIOB, NS_YELLOW_LED_Pin, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(GPIOA, NS_RED_LED_Pin, GPIO_PIN_SET);
	HAL_GPIO_WritePin(GPIOA, EW_GREEN_LED_Pin, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(GPIOA, EW_YELLOW_LED_Pin, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(GPIOA, EW_RED_LED_Pin, GPIO_PIN_SET);
	HAL_GPIO_WritePin(GPIOA, SN_GREEN_LED_Pin, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(GPIOA, SN_YELLOW_LED_Pin, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(GPIOA, SN_RED_LED_Pin, GPIO_PIN_SET);
	HAL_GPIO_WritePin(GPIOB, CROSS_GREEN_LED_Pin, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(GPIOB, CROSS_RED_LED_Pin, GPIO_PIN_SET);
	HAL_Delay(5*1000); //5 seconds
	return SN_Pass_EW_Stop_State;
}

eSystemState CrossHandler(void)
{
	HAL_GPIO_WritePin(GPIOB, NS_GREEN_LED_Pin, GPIO_PIN_SET);
	HAL_GPIO_WritePin(GPIOB, NS_YELLOW_LED_Pin, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(GPIOA, NS_RED_LED_Pin, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(GPIOA, EW_GREEN_LED_Pin, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(GPIOA, EW_YELLOW_LED_Pin, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(GPIOA, EW_RED_LED_Pin, GPIO_PIN_SET);
	HAL_GPIO_WritePin(GPIOA, SN_GREEN_LED_Pin, GPIO_PIN_SET);
	HAL_GPIO_WritePin(GPIOA, SN_YELLOW_LED_Pin, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(GPIOA, SN_RED_LED_Pin, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(GPIOB, CROSS_GREEN_LED_Pin, GPIO_PIN_SET);
	HAL_GPIO_WritePin(GPIOB, CROSS_RED_LED_Pin, GPIO_PIN_RESET);
	HAL_Delay(5*1000); //5 seconds
	return Transition_Cross_EW_State;
	//return All_Stop_EW_State;
}

eSystemState TransitionEWCrossHandler(void)
{
	HAL_GPIO_WritePin(GPIOB, NS_GREEN_LED_Pin, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(GPIOB, NS_YELLOW_LED_Pin, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(GPIOA, NS_RED_LED_Pin, GPIO_PIN_SET);
	HAL_GPIO_WritePin(GPIOA, EW_GREEN_LED_Pin, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(GPIOA, EW_YELLOW_LED_Pin, GPIO_PIN_SET);
	HAL_GPIO_WritePin(GPIOA, EW_RED_LED_Pin, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(GPIOA, SN_GREEN_LED_Pin, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(GPIOA, SN_YELLOW_LED_Pin, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(GPIOA, SN_RED_LED_Pin, GPIO_PIN_SET);
	HAL_GPIO_WritePin(GPIOB, CROSS_GREEN_LED_Pin, GPIO_PIN_RESET);
	//HAL_GPIO_TogglePin(GPIOB, CROSS_GREEN_LED_Pin);
	HAL_GPIO_WritePin(GPIOB, CROSS_RED_LED_Pin, GPIO_PIN_SET);
	HAL_Delay(5*1000); //5 seconds
	return All_Stop_Cross_State;
	//return All_Stop_EW_State;
}
eSystemState TransitionCrossEWHandler(void)
{
	w = 0;
	HAL_GPIO_WritePin(GPIOB, NS_GREEN_LED_Pin, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(GPIOB, NS_YELLOW_LED_Pin, GPIO_PIN_SET);
	HAL_GPIO_WritePin(GPIOA, NS_RED_LED_Pin, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(GPIOA, EW_GREEN_LED_Pin, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(GPIOA, EW_YELLOW_LED_Pin, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(GPIOA, EW_RED_LED_Pin, GPIO_PIN_SET);
	HAL_GPIO_WritePin(GPIOA, SN_GREEN_LED_Pin, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(GPIOA, SN_YELLOW_LED_Pin, GPIO_PIN_SET);
	HAL_GPIO_WritePin(GPIOA, SN_RED_LED_Pin, GPIO_PIN_RESET);
	//HAL_GPIO_WritePin(GPIOB, CROSS_GREEN_LED_Pin, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(GPIOB, CROSS_GREEN_LED_Pin,GPIO_PIN_RESET);
	HAL_GPIO_WritePin(GPIOB, CROSS_RED_LED_Pin, GPIO_PIN_RESET);
	HAL_Delay(5*1000); //5 seconds
	//return All_Stop_Cross_State;
	return All_Stop_EW_State;
}

eSystemState AllStopCrossHandler(void)
{
	HAL_GPIO_WritePin(GPIOB, NS_GREEN_LED_Pin, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(GPIOB, NS_YELLOW_LED_Pin, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(GPIOA, NS_RED_LED_Pin, GPIO_PIN_SET);
	HAL_GPIO_WritePin(GPIOA, EW_GREEN_LED_Pin, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(GPIOA, EW_YELLOW_LED_Pin, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(GPIOA, EW_RED_LED_Pin, GPIO_PIN_SET);
	HAL_GPIO_WritePin(GPIOA, SN_GREEN_LED_Pin, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(GPIOA, SN_YELLOW_LED_Pin, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(GPIOA, SN_RED_LED_Pin, GPIO_PIN_SET);
	HAL_GPIO_WritePin(GPIOB, CROSS_GREEN_LED_Pin, GPIO_PIN_RESET);
	//HAL_GPIO_TogglePin(GPIOB, CROSS_GREEN_LED_Pin);
	//HAL_Delay(500);
	HAL_GPIO_WritePin(GPIOB, CROSS_RED_LED_Pin, GPIO_PIN_SET);
	HAL_Delay(5*1000); //5 seconds
	//return NS_Stop_EW_Pass_State;
	//return All_Stop_EW_State;
	return Cross_State;
}
eSystemState TransitionSouthNorth_NorthSouthHandler(void)
{
	HAL_GPIO_WritePin(GPIOB, NS_GREEN_LED_Pin, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(GPIOB, NS_YELLOW_LED_Pin, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(GPIOA, NS_RED_LED_Pin, GPIO_PIN_SET);
	HAL_GPIO_WritePin(GPIOA, EW_GREEN_LED_Pin, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(GPIOA, EW_YELLOW_LED_Pin, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(GPIOA, EW_RED_LED_Pin, GPIO_PIN_SET);
	HAL_GPIO_WritePin(GPIOA, SN_GREEN_LED_Pin, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(GPIOA, SN_YELLOW_LED_Pin, GPIO_PIN_SET);
	HAL_GPIO_WritePin(GPIOA, SN_RED_LED_Pin, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(GPIOB, CROSS_GREEN_LED_Pin, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(GPIOB, CROSS_RED_LED_Pin, GPIO_PIN_SET);
	HAL_Delay(5*1000); //5 seconds
	return All_Stop_NS_State;
}
eSystemState TransitionNorthSouth_SouthNorthHandler(void)
{
	HAL_GPIO_WritePin(GPIOB, NS_GREEN_LED_Pin, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(GPIOB, NS_YELLOW_LED_Pin, GPIO_PIN_SET);
	HAL_GPIO_WritePin(GPIOA, NS_RED_LED_Pin, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(GPIOA, EW_GREEN_LED_Pin, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(GPIOA, EW_YELLOW_LED_Pin, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(GPIOA, EW_RED_LED_Pin, GPIO_PIN_SET);
	HAL_GPIO_WritePin(GPIOA, SN_GREEN_LED_Pin, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(GPIOA, SN_YELLOW_LED_Pin, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(GPIOA, SN_RED_LED_Pin, GPIO_PIN_SET);
	HAL_GPIO_WritePin(GPIOB, CROSS_GREEN_LED_Pin, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(GPIOB, CROSS_RED_LED_Pin, GPIO_PIN_SET);
	HAL_Delay(5*1000); //5 seconds
	return All_Stop_SN_State;
}
//int w;
/**
* @brief The application entry point.
*
* @retval None
*/
int main(void)
{
/* MCU Configuration----------------------------------------------------------*/
/* Reset of all peripherals, Initializes the Flash interface and the Systick. */
HAL_Init();
/* Configure the system clock */
SystemClock_Config();
/* Initialize all configured peripherals */
MX_GPIO_Init();
MX_USART2_UART_Init();
/*Declare eNextState and initialize it to All Stop North South */
//eSystemState eNextState = Transition_NS_State;
eSystemState eNextState = NS_Stop_EW_Pass_State;
/* Infinite loop */
while (1)
{
	int i = HAL_GPIO_ReadPin(GPIOB, NS_SWITCH_Pin);
	int j = HAL_GPIO_ReadPin(GPIOA, SN_SWITCH_Pin);
	if(HAL_GPIO_ReadPin(GPIOA, CROSS_SWITCH_Pin))
	{
		w = 1;
	}
switch(eNextState)
{
case Transition_NS_State:
if(j == 1)
{
	eNextState = TransitionNorthSouth_SouthNorthHandler();
}
else
{
	eNextState = TransitionNorthSouthHandler();
}
break;
case NS_Pass_EW_Stop_State:
eNextState = NorthSouthPassHandler();
break;
case All_Stop_EW_State:
eNextState = AllStopEastWestHandler();
break;
case NS_Stop_EW_Pass_State:
if(i == 1)
{
	eNextState = AllStopNorthSouthHandler();
}
else
{
eNextState = EastWestPassHandler();
}
break;
case Transition_EW_State:
//if(HAL_GPIO_ReadPin(GPIOB,NS_SWITCH_Pin))
	if(i == 1)
{
	eNextState = TransitionEastWestNSHandler();
	//i = 0;
}
//if(HAL_GPIO_ReadPin(GPIOA,SN_SWITCH_Pin))
	if(j == 1)
{
	eNextState = TransitionEastWestSNHandler();
	//j = 0;
}
if(w == 1)
{
	eNextState = TransitionEWCrossHandler();
}
break;
case All_Stop_NS_State:
eNextState = AllStopNorthSouthHandler();
break;
case Transition_SN_State:
if( i ==1 )
{
	eNextState = TransitionSouthNorth_NorthSouthHandler();
}
else
{
	eNextState = TransitionSouthNorthHandler();
}
break;
case SN_Pass_EW_Stop_State:
eNextState = SouthNorthHandler();
break;
case All_Stop_SN_State:
eNextState = AllStopSNHandler();
break;
case Cross_State:
eNextState = CrossHandler();
break;
case Transition_Cross_EW_State:
eNextState = TransitionCrossEWHandler();
break;
case All_Stop_Cross_State:
eNextState = AllStopCrossHandler();
break;
default:
eNextState = AllStopNorthSouthHandler();
break;
}
}
/* USER CODE END 3 */
}
/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_NONE;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_HSI;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_0) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief USART2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART2_UART_Init(void)
{

  /* USER CODE BEGIN USART2_Init 0 */

  /* USER CODE END USART2_Init 0 */

  /* USER CODE BEGIN USART2_Init 1 */

  /* USER CODE END USART2_Init 1 */
  huart2.Instance = USART2;
  huart2.Init.BaudRate = 38400;
  huart2.Init.WordLength = UART_WORDLENGTH_8B;
  huart2.Init.StopBits = UART_STOPBITS_1;
  huart2.Init.Parity = UART_PARITY_NONE;
  huart2.Init.Mode = UART_MODE_TX_RX;
  huart2.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart2.Init.OverSampling = UART_OVERSAMPLING_16;
  huart2.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
  huart2.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
  if (HAL_UART_Init(&huart2) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART2_Init 2 */

  /* USER CODE END USART2_Init 2 */

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOF_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOA, EW_GREEN_LED_Pin|EW_YELLOW_LED_Pin|EW_RED_LED_Pin|SN_GREEN_LED_Pin 
                          |SN_YELLOW_LED_Pin|SN_RED_LED_Pin|NS_RED_LED_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOB, CROSS_GREEN_LED_Pin|NS_GREEN_LED_Pin|NS_YELLOW_LED_Pin|CROSS_RED_LED_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pins : EW_GREEN_LED_Pin EW_YELLOW_LED_Pin EW_RED_LED_Pin SN_GREEN_LED_Pin 
                           SN_YELLOW_LED_Pin SN_RED_LED_Pin NS_RED_LED_Pin */
  GPIO_InitStruct.Pin = EW_GREEN_LED_Pin|EW_YELLOW_LED_Pin|EW_RED_LED_Pin|SN_GREEN_LED_Pin 
                          |SN_YELLOW_LED_Pin|SN_RED_LED_Pin|NS_RED_LED_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pins : CROSS_GREEN_LED_Pin NS_GREEN_LED_Pin NS_YELLOW_LED_Pin CROSS_RED_LED_Pin */
  GPIO_InitStruct.Pin = CROSS_GREEN_LED_Pin|NS_GREEN_LED_Pin|NS_YELLOW_LED_Pin|CROSS_RED_LED_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /*Configure GPIO pins : SN_SWITCH_Pin CROSS_SWITCH_Pin */
  GPIO_InitStruct.Pin = SN_SWITCH_Pin|CROSS_SWITCH_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pin : NS_SWITCH_Pin */
  GPIO_InitStruct.Pin = NS_SWITCH_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(NS_SWITCH_GPIO_Port, &GPIO_InitStruct);

}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */

  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(char *file, uint32_t line)
{ 
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
