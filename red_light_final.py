# PiCamera imports
from picamera import PiCamera
from time import sleep

#external mod imports
import RPi.GPIO as GPIO
import time

#GPIO.setwarnings(False)

#pin definitions
cameraSwitch= 4
redLight =22 # pin 22 / Pi pin 15

#pin setup
GPIO.setmode(GPIO.BCM)
GPIO.setup(cameraSwitch,GPIO.IN)
GPIO.setup(redLight,GPIO.IN)

#initial state
#GPIO.output(cameraSwitch,GPIO.LOW)
#setup email
import email, smtplib,ssl

from email import encoders
from email.mime.base import MIMEBase
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

subject = "An email with attachment from Python"
body = "Red Light Triggered"
sender_email = "cpe185.redlight@gmail.com"
receiver_email = "ekv.gamboa@gmail.com"
password = "bullshit185-"


message = MIMEMultipart()
message["From"] = sender_email
message["To"] = receiver_email
message["Subject"] = subject


message.attach(MIMEText(body,"plain"))

filename = "redlight.jpg"

with open(filename,"rb") as attachment:
	part = MIMEBase("application","octect-stream")
	part.set_payload(attachment.read())
	
encoders.encode_base64(part)

part.add_header("Content-Disposiition","attachment; filename = {filename}")

message.attach(part)
text = message.as_string()
#camera setup
camera = PiCamera()
print("testing camera. Press CTRL+C to exit")
try:
    while 1:
        if GPIO.input(redLight) and GPIO.input(cameraSwitch):
        #if GPIO.input(cameraSwitch):
            print("button")
            camera.start_preview()
            sleep(2)
            camera.capture('/home/pi/Desktop/redlight.jpg')
            camera.stop_preview()
            context = ssl.create_default_context()
            with smtplib.SMTP_SSL("smtp.gmail.com",465,context=context) as server:
                    server.login("cpe185.redlight@gmail.com",password)
                    server.sendmail(sender_email,receiver_email,text)
except KeyboardInterrupt:
    GPIO.cleanup()
