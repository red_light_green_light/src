/*
 * crosstone.c:
 *	
 * This program causes a tone to play through a piezo speaker using 
 * a raspberry pi when a switch is pressed.
 ***********************************************************************
 */

#include <stdio.h>
#include <errno.h>
#include <string.h>

#include <wiringPi.h>
#include <softTone.h>

#define	PIN	3
int crossPin = 27; 
int scale[8] = {262, 500, 262, 500, 262, 500, 262, 500};

int main ()
{
  int i;
  wiringPiSetupGpio(); // Initialize wiringPi
  pinMode(crossPin, INPUT); // Set sensor as INPUT
  

  wiringPiSetupGpio();

  softToneCreate(PIN);

  while(1)
  {
	if (digitalRead(crossPin))
	{
	printf ("cross is on\n");

		for (i = 0 ; i < 8 ; ++i)
		{
		printf ("%3d\n", i) ;
		softToneWrite(PIN, scale[i]);
		delay (500) ;
		}		
    }
	else
	{
		softToneWrite(PIN,0);
		delay(500);
	}
  }
return 0;
}
